

App To Do 
=============
immediate fixes version 0.1.1

fixed "unprinted labels" are not partitioned by teams
fixed: Prevent Site deletion if events/specimens like events page
fixed : better message when deletion blocked (has child records)
fixed : show more user log-in registration data in user table

add home coordinates to teams.  When creating a team, enter a decimal lat and long coordinate for the center of the sampling region.  There is no map 



Bugs or problems

No realy auditing capability. 
papertrail gem with admin interface to see historical 
undo interface?  

* see guy with problem http://stackoverflow.com/questions/15897760/jquery-datatables-rails-bootstrap-styling?rq=1
* see pagination http://stackoverflow.com/questions/12684328/rails-bootstrap-datatables
* troubleshooting  : http://stackoverflow.com/questions/12196759/gem-jquery-datatables-rails-not-working?rq=1



CSV export doesn't include site data
american date format input doesn't work
Species list does not have new species or additional taxonomy; update species list with Jason's list, maintaining ITIS numbers
when making new specimen in event show page, default collectors to those listed on the parent collection event
can add collectors to a specimen (comma list) but not linked to collectors; allows for non-user names but then doesn't work.
go staight to dashboard after log-in (not home page)

no basic reporting...
Can't list specimens species, or show a species map or timeline
collectors can't see their own specimens  (could search by name in specimen table and export)



Version 0.2

Species Table performance is slow when large
    use AJAX if it will do proper sorting see Kaminari pagination SO post map datatables vars to Kamaniri params page and total entries
species updating combursome: species update modal form on species table
    add bootstrap modal ajax to erb template with no id or other data
    javascript button inserts these values in hidden field
    javascript submit also updates table which means a redraw

no differentiation of specimens outside of treatment and site or comments.  allow 'tagging' of specimens and sites


can't add new host plants now. allow team manager
no collection notes, start with pinned specimens

Reporting page
---------------
very basic, leave to those exporting data

* total specimens collected
* total that have species filled in
* team list
 count users per team
specimens per team
per team?
taxnonomic summary page with 'un-determined' count

**export**
expand CSV to universal relation; do this for a specimens table too

Species report : one team/all teams

Determination Page
----------------

* :sites, specimens
* gem  Taggable     



Site pages and data
-----
validations!
taggable


for the in-site locations:
* add a mechanism for restricting editing of in-site list IF specimens are specimens are using these
* OR warning not to edit
* OR special form to edit this list

site map
* markers have info bubble - future
* markers with number in them? http://stackoverflow.com/questions/2890670/google-maps-place-number-in-marker

better - click a marker and rather than take to site page
http://addyosmani.github.io/jquery-ui-bootstrap/map.html


add index and validation to restrict adding c-event on same day same location..?
on 'new' form list existing collecting events for that date...


Specimens
---------
validations
taggable

new recod default data collectors from collection event


in collection event 
* show # of specimens added
* [tags] [collectors] [add specimens] 


apply something to multple checked items
* change all collectors
* change/enter host plants
* location/status
* tags


When showing the specimen, have a more complete specimen show page in reporting section

pull in data from associations

* place: site (map) and details (hash)
* event details (hash)
* determination (det. status, determiner, date, species



 
UI /Layouts
---


helpers for buttons, with "type" edit/delete/etc  that have default icons for diff types of buttons

add good title helper with right side region for buttons etc

on log-on, go to team data collection overview page (make this easy to change)


unless admin, then go somewhere else

move the team members link into a "manage team" link

Taggable  https://github.com/mbleigh/acts-as-taggable-on helpers for bootstrap labels  ?

http://aehlke.github.io/tag-it/

<a href="#" class="btn btn-info">TagText&nbsp;<i class="icon-remove-circle"></i></a>  or icon-remove

better : http://welldonethings.com/tags/manager

  <input type="text" name="tags" placeholder="Tags" class="tagManager"/>
  ... 
  <script type="text/javascript" charset="utf-8">
      jQuery(".tagManager").tagsManager();
  </script>


URLS currently use numeric IDs.  Use names (e.g. team name) instead

https://github.com/FriendlyId/friendly_id


User
----

validations
restrict role assignment, can? :manage, User works for all users
get email working on staging server

Teams
----

better view of sites/events on team page. 
Map of sites on team page

thinlk this is done : correct ability.rb manager actions 
* can list teams/sites even when not a manager

Generalize
----------

add site title helper, minimize changes in site name
move footer links into a helper

Lookup tables : collection_type, State/Province, 

help
----

there should be a menu item and section with help files.  Minimally, more information for folks who want to sign up.  Second more info for managers, or how to use the collection system. 

ideas 
* member routes for all models  resource/help



colleague
----------

create colleague role
potentially, genericize the managers_controller to the roles_controller

add per Team form to select colleague (from non-team users)
add ability for read-only per colleague
colleagues exist in roles only



FUTURE
======
sites map : side by side with site info 