class Team < ActiveRecord::Base
  attr_accessible :affiliation, :name, :notes, :url
  
  has_many :users, :order => "lastname, firstname" 
  accepts_nested_attributes_for :users
  
  has_many :sites
  has_many :collection_events, :through =>:sites
  has_many :specimens, :through => :collection_events
    
  accepts_nested_attributes_for :sites
  
  validates :name, :presence => true

  def to_s
    self.name
  end
  
  def managers
    self.users.managers
  end
  
  def manager_list
    self.managers.map {|m| m.name }.join(", ")
  end
  
  def member_count
    self.users ? self.users.count : 0 
  end
  
  def members
    if users
      self.users - self.users.managers
    end
  end
  
  def add_manager(user)
    if user.team_id == self.id && user.registered? 
      user.add_role :manager, self
    end
  end
  
  def remove_manager!(user)
    if self.managers.include?(user)
      user.remove_role :manager, self
    end
  end
  
  
end
