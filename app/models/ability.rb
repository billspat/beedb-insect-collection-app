class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin
      can :manage, :all
      return
    end
    
    can :read, Species
    can [:read,:search], Species
    
    can :read, User, :team_id => user.team_id
    can :update, User, :id => user.id
    can :read, Team,:id => user.team_id
    
    if user.manager?
      can :manage, Team,  :id => user.team_id 
      can :manage, User, :team_id => user.team_id
    end 
        
    can :manage, Site, :team_id => user.team_id
    
    # users can manage events from their team sites, without checking team in the event model
    
    can :manage, CollectionEvent do |cevent| 
      cevent.site.team.id == user.team_id
      #:site => {:team_id => user.team_id}
    end
    # can :manage, Specimen, :site => {:team_id => user.team_id}
        
    can :manage, Specimen do |specimen|
        specimen.collection_event.site.team_id == user.team_id
    end
    
  end

end



# Define abilities for the passed in user here. For example:
#
#   user ||= User.new # guest user (not logged in)
#   if user.admin?
#     can :manage, :all
#   else
#     can :read, :all
#   end
#
# The first argument to `can` is the action you are giving the user permission to do.
# If you pass :manage it will apply to every action. Other common actions here are
# :read, :create, :update and :destroy.
#
# The second argument is the resource the user can perform the action on. If you pass
# :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
#
# The third argument is an optional hash of conditions to further filter the objects.
# For example, here the user can only update published articles.
#
#   can :update, Article, :published => true
#
# See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
