class Species < ActiveRecord::Base
  attr_accessible :common_name, :common_synonyms, :genus, :notes, :species, :sub_species, :taxonomic_order, :subfamily, :taxonomic_family, :superfamily, :tribe, :tempname, :itis_tsn,:taxon_author
  
  has_many :specimens
  
  default_scope order('genus,species,sub_species')

  # require basic taxnomony including species
  # if species is required
  validates :genus, :species,:taxonomic_family, :taxonomic_order, :presence => true
  validates :species, :uniqueness => { :scope => :genus, :message => "genus species must be unique (already in table)" }
   
  #  DISPLAY
  def to_s
    scientific_name + " ( #{taxonomic_family})"
  end
  
  def scientific_name
    sn = "#{genus.capitalize} #{species}"
    sn << " #{sub_species}" if self.sub_species?
    sn << " #{tempname}"    if self.tempname?
    sn
  end
  
  def to_hash
      {"scientific name" => scientific_name, 
        "order" => taxonomic_order.capitalize, 
        "family" => taxonomic_family.capitalize,
        "common name" => common_name, 
        "synonyms" =>  common_synonyms}
  end
  
  # custom searches
  def self.where_scientific_name_like(q)
    q.gsub!(/\s+/,'')
    where("genus like ? or species like ? or( #{connection.concat('genus','species')}) like ?", "%#{q}%","%#{q}%","%#{q}%").limit(250)
  end

  # other ideas
  # spaces = /\s+/    
  # q.split(spaces).inject([]) do |collection,term|
  #       collection + where('genus like ? or species like ?', "%#{term}%","%#{term}%")
  #  end 
  
  # def self.where_in_scientific_name(q)
  #   # strip percents and spaces from q
  #   where('genus like ? or species like ?', '%' + q + '%')
  # end
  # 
  # def matches_scientific_name(q)
  #   found = q.split(" ").map {|term| self.genus =~ Regexp.new(term) || self.species =~ Regexp.new(term) }.inject {|found, match| found || match}
  #   found != nil
  # end

    
end


