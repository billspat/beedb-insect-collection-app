class Site < ActiveRecord::Base
  
  attr_accessible :active, :elevation, :latitude, :longitude, :locality, :name, :note, :team_id, :county, :stateprov, :country, :treatment_id, :loc_list, :lat, :lng
  
  belongs_to :team
  has_many :collection_events, :dependent => :restrict
  belongs_to :treatment
  has_many :specimens, :through => :collection_events
  
  accepts_nested_attributes_for :collection_events
    
  def gmaps4rails_address
    self.locality
  end
  
  def self.countries
    ["US", "Canada","Mexico"]
  end
    
  validates :name,      :presence => true
  validates :treatment, :presence => true
  validates :stateprov, :presence => true
  validates :country,   :presence => true
  
  validates_associated :team

  # temp while transitioning  
  alias_attribute :lat, :latitude
  alias_attribute :lng, :longitude

  def to_s
    name
  end
  
  def coordinates(precision=4)
    "#{latitude.round(precision)},#{longitude.round(precision)}" unless (latitude.blank? || longitude.blank?)
  end
  
  def decorated_coordinates(precision=3)
    "N#{latitude.round(precision)},W#{longitude.round(precision)}" unless (latitude.blank? || longitude.blank?)
  end
  
  
  def to_hash
    { "Site Name"               => name ,
      "Treatment"               => (treatment.name if treatment),
      "Coordinates (lat,lng)"   => coordinates,
      "Elevation (m)"           => "#{elevation}m" ,
      "Locality"                => locality,
      "County/Subdiv"           => county,
      "State"                   => "#{stateprov},   #{country}",
      "Collect Locs"            => loc_list, 
      "Team"                    => team.name,
      "Note"                    => note  }
    end
    
  # convert the list of locations into array, split on , or ;
  def loc_array
    loc_list.split(/,|;/).map{|l| l.strip} if loc_list?
  end
   
  
end
