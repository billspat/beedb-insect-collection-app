class Treatment < ActiveRecord::Base
  attr_accessible :active, :name, :notes, :treatment_code, :year
  
  has_many :sites
  
  def to_s
    "#{self.name} (#{self.year})"
  end
  
end
