# require 'lib/model_to_csv.rb'
class Specimen < ActiveRecord::Base
  
  include ModelToCSV
  
  attr_accessible :sex,:collection_event_id, :collectors, :determination_status, :determiner, :hostplant_id, :hostplant_literal, :idcode, :label_printed_on, :location, :notes, :status,:species_id,:taxa
  belongs_to :collection_event
  belongs_to :species
  belongs_to :hostplant

  attr_accessor :bulk_insert_count
  attr_accessible :bulk_insert_count
  
  scope :unprinted, -> {where('label_printed_on IS NULL')}
  scope :undetermined, -> {where('species_id IS NULL')}
  scope :determined, -> {where('species_id IS NOT NULL')}
  
  delegate :site, :to => :collection_event, :allow_nil => true
  
  validates :collectors, :presence => true
  validates_associated :collection_event
  
  # create a bag of n identical new specimens
  def self.bulk_new(n = 1, specimen_params)
    new_specimens = []
  
    if (n.is_a? Integer) && n > 0
      (1..n).each do 
        begin       
          new_specimens << Specimen.new(specimen_params)
        rescue
          exit
        end
      end
    end  
    
    new_specimens  
  end


  # bulk save a bunch of specimens, wrapped in a transaction 
  def self.bulk_save(specimens)
    ActiveRecord::Base.transaction do
      begin
        specimens.each do |specimen|
          specimen.save
        end    
      rescue
        exit
      end
    end
  end
  
  
  def collection_site
    self.collection_event.site
  end
  
  # special display of specimen IDs to make them unique for this project
  
  def team
    collection_event.site.team
  end
  
  def coded_id
    project_prefix + sprintf("%04d", self.id) if self.id?
  end
  
   
  def shortened_geoname
     s = LocShortener.shorten("#{site.country}: #{site.stateprov}#{(', ' + site.county if site.county?)}" )
     s.truncate(30,:omission=>".")
  end
  
  def shortened_locality(n=30)
    locality = self.collection_event.site.locality
    LocShortener.shorten(locality).truncate(n, :seperator=>' ', :omission =>".")
  end
  
  def shortened_collectors
    collectors.blank? ? "" : collectors.truncate(20, :separator=>",", :omission=>",et al")
  end
  
  def shortened_location(n = 5)
    location.blank? ? "" :location.truncate(n, :omission=>".")
  end
  
  def host_plant_str
    if hostplant
      hostplant.scientific_name
    else
      hostplant_literal
    end
  end
  
  def site_str
    site = collection_event.site
    site.to_s if site
  end
  
  def scientific_name_str
    species.blank? ? 'undetermined' : species.scientific_name
  end
  
  def set_print_date(printdate = Time.now)
     update_attribute("label_printed_on",printdate.strftime("%d/%m/%Y"))
  end

  def site_name_str
    site = self.site
    site.name if site
  end
  
  def export_data 
    collection_event ||= self.collection_event
    site ||= self.specimen.site
      {
      "Specimen ID Code"      => self.coded_id,
      "Collection Site"       => self.site_name_str,
      "State/Prov"            => self.site.stateprov,
      "Country"               => self.site.country,
      "Treatment"             => self.site.treatment.to_s,
      "Collection on"         => self.collection_event.date_start,
      "Collection end date"   => self.collection_event.date_end,
      "Collection time"       => self.collection_event.collection_time,
      "Collected by"          => self.specimen.collectors,
      "Host plant"            => self.hostplant_str,
      "Species"               => self.scientific_name_str,
      "Sex"                   => self.sex,
      "Specimen Status"       => self.status,
      "Location"              => self.location,
      "Determination status"  => self.determination_status,
      "Determiner"            => self.determiner,
      "Notes"                 => self.notes
     }
   end
   
   def csv_headers(x)
     export_data.keys
   end
  
   def csv_dump(header_val)
     export_data.values
   end
  

  # CLASS METHODS
  
  def self.to_hash
    # # all.to_a.map(&:serializable_hash)
  end
  
 
  
  def self.export_template
    {
    "Specimen ID Code"      =>'coded_id',
    "Collection on"         =>'collection_event',
    "Collected by"          =>'collectors',
    "Host plant"            =>'host_plant_str',  
    "Species"               =>'scientific_name_str',
    "Sex"                   =>'sex',
    "Status"                =>'status',
    "Location"              =>'location',
    "Determination status"  =>'determination_status',
    "Determiner"            =>'determiner',
    "Notes"                 =>'notes',
     "Collection Site"      =>'site.name',
     "State/Prov"           =>'site.stateprov',
     "Treatment"            =>'site.treatment.to_s'
   }
  end
  
  def self.export_header
    Specimen.export_template.keys
  end
  
  # def self.unprinted(team=nil)
  #   if team.blank?
  #       self.where("label_printed_on IS NULL")
  #     else
  #       team.specimens.where("label_printed_on IS NULL")
  #     end
  #     
  # end
  
  def export_data
    Specimen.export_template.values.map { |meth| self.send(meth).to_s}
  end
  
  
  def csv_headers
    export_data.keys
  end
  
  def csv_dump
    export_data.values
  end
  
  
  def to_hash(options={})
    {
    "Specimen ID Code"      => self.coded_id,
    "Collection on"         => self.collection_event.to_s,
    "Collected by"          => "#{self.collectors}, #{self.team.to_s}",
    "Host plant"            =>  self.hostplant.to_s,  
    "Species"               => species_id? ? "<i>#{self.species.to_s}</i>" : "undetermined",
    "Sex"                   => self.sex,
    "Label printed on"      => self.label_printed_on ,
    "Status"                => self.status ,
  "Site Collection Location"  => self.location ,
    "Determination status"  => self.determination_status ,
    "Determiner"            => self.determiner ,
    "Notes"                 => self.notes }
    
     # self.hostplant_id + " " + 
  end
  
  def to_s 
     "#{coded_id} collected #{self.collection_event.to_s}"
  end
  
  # to do:
  # create a project table and use prefix from that for this value
  # store this value along with ID in a non-mutable table column for safe keeping and auditing
  def project_prefix
      "ICP" +  two_digit_year
  end
  
  def insert_year_digits
    self.date_created.strftime("%y")
  end
  
  def two_digit_year
    # to fix labels printed with current year because of poor programming, 
    # the project prefix year is based on date the record is created, not date specimen was collected
    # future project prefixes should not include a year
    if created_at.blank? 
      "  "
    else
      created_at.strftime("%y")
    end    
  end
  
end
