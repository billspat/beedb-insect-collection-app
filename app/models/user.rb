class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, # :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :team_id, :role_ids, :name, :email, :password, :password_confirmation, :remember_me, :firstname,  :lastname, :affiliation, :position, :contact,  :added_by_id, :notes, :as => :admin  
  attr_accessible :team_id, :role_ids, :name, :email, :password, :password_confirmation, :remember_me, :firstname,  :lastname, :affiliation, :position, :contact,  :added_by_id, :notes
  
  validates :firstname, :presence => true
  validates :lastname, :presence => true
  validates_associated :team                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
  
  belongs_to :team
  
  default_scope :order => 'lastname, firstname'
  scope :managers, lambda { 
      joins(:roles).where("roles.name=?", 'manager')
    }
    
  before_validation(:on => :create) do
     self.password = Devise.friendly_token[0,20] if self.password.nil? 
     add_default_role
  end
  
  def name
    "#{firstname} #{lastname}"
  end
  
  def collector_name
    "#{firstname[0]}. #{lastname}"
  end
  
  def name=(bothnames)
    self.firstname, self.lastname = bothnames.split(" ")
  end
  
  def registered?
    self.invited_to_sign_up? || self.invitation_accepted?
  end
  
  def invitable?
    self.email? && !self.invitation_accepted? # removing this condition as confirmable option removed && !self.confirmed_at?
  end

  def add_default_role
    self.add_role :user if self.roles.empty?
  end
  
  def manager?
    self.roles_name.include?("manager")
  end
  
  # HACKS to deal with unintential upgrade to Devise 3 and Devise_invtable 1.3 which have different fields
  # and different methods.  
  
  # BETTER => USE A METHOD ALIAS
  def invitation_created_at
    self.invitation_sent_at
  end
  
  # this is added just to make the model compatible with Devise 3
  def invitation_created_at=(date)
    self.invitation_sent_at = date
  end

  #  override devise_invitable method to work with the truncated 60char keys        
  #  can't just truncate all tokens since I removed the limit on the token field for future invites

  def self.find_by_invitation_token(original_token, only_valid)
    invitation_token = Devise.token_generator.digest(self, :invitation_token, original_token)

    invitable = find_or_initialize_with_error_by(:invitation_token, invitation_token)
    
    # HACK to deal with LIMIT 60 shortened tokens
    if !invitable.persisted? 
      invitable = User.find_or_initialize_with_error_by(:invitation_token, invitation_token[0..59])
    end
    
    if !invitable.persisted? && Devise.allow_insecure_token_lookup
      invitable = find_or_initialize_with_error_by(:invitation_token, original_token)
    end
    invitable.errors.add(:invitation_token, :invalid) if invitable.invitation_token && invitable.persisted? && !invitable.valid_invitation?
    invitable.invitation_token = original_token
    invitable unless only_valid && invitable.errors.present?
  end
  
end

