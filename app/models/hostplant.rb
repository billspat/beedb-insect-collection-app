class Hostplant < ActiveRecord::Base
  attr_accessible  :common_name,  :description,  :taxonomic_family,  :genus,  :species,  :synonyms
  
  has_many :specimens
  
  def to_s
    "#{common_name} (#{genus} #{species})"
  end
  
  def scientific_name
    "#{genus} #{species}"
  end
  
  
end
