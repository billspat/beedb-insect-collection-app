class CollectionLocation < ActiveRecord::Base
  attr_accessible :description, :location, :value, :year
  
  default_scope order('value,location')
  default_scope where(:year => Time.now.year.to_s)
  
end
