class CollectionEvent < ActiveRecord::Base
  belongs_to :site
  # belongs_to :team, :through => :site
  attr_accessible :site_id, :collection_time, :collection_type_id, :collection_type_text, :collectors, :date_end, :date_start, :date_verbatim

  has_many :specimens, :dependent => :restrict  # raise error if destroy when problems arise
  
  validates :date_start, :presence => true
  
  validates_associated :site
  
  alias_attribute :date, :date_start
  
  default_scope {order('site_id, date_start')}
  # don't make the user always set the site, if there isn't one. 
  
  before_validation(:on => :create) do
    if self.team_id.blank? && self.site && self.site.team_id?
       self.team_id = self.site.team_id
    end
  end
  
  # for simple calendar gem
  def start_time
    ctime =  collection_time || Time.now
      
    DateTime.new(date_start.year, date_start.month,
                date_start.day, ctime.hour,
                ctime.min, ctime.sec)
  end
  
  def cal_display
    "#{self.site.name} (#{self.specimen_count})"
  end
  

  def to_s
    "#{self.short_date}  @ #{self.site.name}"
  end

  #   display code
  
  def end_date_to_s
  
  end
  
  def to_full_date
    "#{short_date_format(date_start)}#{(' -'+short_date_format(date_end)) unless self.date_end.blank?} #{"; " + self.time_display if collection_time}"
  end
 
  def dates_for_table
    date_start.to_s + (("-" + date_end.to_s) if self.date_end? )
  end
  
  def short_date_format(d=self.date_start)
    d.blank? ? "" : d.strftime("%Y-%m-%d")
  end
  
  def long_date_format(d = self.date_start)
    d.strftime("%d %B, %Y")
  end
  
  def date_start_long
    long_date_format(date_start)
  end
  
  def date_end_long
    date_end? ? long_date_format(date_end) : ""
  end
  
  def date_start_short
    short_date_format(date_start)
  end
  
  def short_date
    short_date_format()
  end

  def time_display
    collection_time.strftime('%I:%M %p')
  end
  
  def to_hash
    {
      "site" => site.to_s,
      "date #{'; time' if collection_time}" => self.to_full_date,
      "collectors" => collectors,
      "collection type" => collection_type_text,
      "notes" => ""   
    }
  end
  
  def specimen_count
    ( specimens.count unless self.specimens.blank? ).to_i
  end
  


end
