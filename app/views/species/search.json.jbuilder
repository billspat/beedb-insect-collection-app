# basic json  for jquery select2 when species are selected
json.array!(@species) do |json, s|
  json.text s.scientific_name
  json.id s.id
end
    