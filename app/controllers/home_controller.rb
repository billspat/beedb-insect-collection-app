class HomeController < ApplicationController
  skip_before_filter :require_sign_in
  
  def index
    @users = User.all
  end
end
