class ManagersController < ApplicationController

  before_filter :get_team!
  
  def index
    @managers = @team.managers
    @candidates = @team.members.select {|m| m.registered? }
    # view?
  end
  
  def show
    @manager = @team.managers.find(:id)
    # view?  
  end

  #  this isn't really needed...
 
  def edit
    authorize! :update, @team 
    @managers = @team.managers
    @user = @managers.find(:id)
  end
  
  def new
    # select a new manager from existing members
    @candiates = @team.members.select {|m| m.registered? }
  end
  
  # add a manager user_id for gien team id
  def update  
    authorize! :update, @team 
       
    @user = User.find(params[:id]) # and user is team member?
   
    respond_to do |format|
      if @user && @team.add_manager(@user)
        format.html { redirect_to @team, notice: "#{@user.name} added as manager" }
        format.json { head :no_content }
      else
        format.html { redirect_to @team, notice: "general error: manager not added" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end 

  def destroy
    authorize! :update, @team 
    @user = User.find(params[:id]) # and user is team member?
    
    respond_to do |format|
      if @user && @team.remove_manager!(@user)
        format.html { redirect_to @team, notice: "#{@user.name} removed as manager" }
        format.json { head :no_content }
      else
        format.html { redirect_to @team, notice: "general error: manager not removed" }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
  def get_team!
    @team = Team.find(params[:team_id])
    if @team.nil?
      redirect_to root_url, notice: "No team found"
    end
  end

end
