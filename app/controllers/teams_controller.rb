class TeamsController < ApplicationController
 load_and_authorize_resource

 def new
 end
 
 def create
   @team = Team.new(params[:team])
   
   respond_to do |format|
     if @team.save
       format.html { redirect_to teams_path, notice: "New team '#{@team.name}' was successfully created." }
       format.json { render json: @team, status: :created, location: @team }
     else
       format.html { render action: "new" }
       format.json { render json: @team.errors, status: :unprocessable_entity }
     end
   end
 end
 

 def update
   @team = Team.find(params[:id])

   respond_to do |format|
     if @team.update_attributes(params[:team])
       format.html { redirect_to @team, notice: 'Team was successfully updated.' }
       format.json { head :no_content }
     else
       format.html { render action: "edit" }
       format.json { render json: @team.errors, status: :unprocessable_entity }
     end
   end
 end 
 
 def add_manager
   @team = Team.find(params[:id])
   
   authorize! :update, @team    
   @user = User.find(params[:user_id]) # and user is team member?
   
   respond_to do |format|
     if @user && @team.add_manager(@user)
       format.html { redirect_to @team, notice: "#{@user.name} added as manager" }
       format.json { head :no_content }
     else
       format.html { redirect_to @team, notice: "general error: manager not added" }
       format.json { render json: @team.errors, status: :unprocessable_entity }
     end
   end
   
 end 
end
