class SpecimensController < ApplicationController
  include SpecimensHelper
  
  load_and_authorize_resource :team
  # load_and_authorize_resource :specimen, through: :team

  def index
    @team = current_team
    @specimens = []
    # @specimens = @team.specimens.all
    if params[:collection_event_id] && @collection_event = CollectionEvent.find(params[:collection_event_id])
       @specimens = @collection_event.specimens.all
    elsif params[:ids]
      unless (id_list=params[:ids].split(',')).blank?
         @specimens = Specimen.find(id_list) 
      end
     
    elsif params[:site_id] && @site = Site.fin(params[:site_id])
       @specimens = @site.specimens.all
    else
       @specimens = @team.specimens.all
    end 
    
    #  search/sort ?
    respond_to do |format|
      format.html  
      format.xls
      format.xlsx  
      format.json { render json: @specimens }
      format.csv  { render :text => SpecimenExporter.to_csv(@specimens) unless @specimens.empty? }
      format.pdf  do
        send_pdf_labels(@specimens, params[:label_type] )
      end
    end
  end

  # PUT team/specimens/print
  def print_labels
    @team = current_team
    if params[:id]
      @specimens = [Specimen.find(params[:id].split(","))]
    else
      @specimens = @team.specimens.unprinted
    end

    if @specimens.empty?
      redirect_to :back, alert:  "No specimens with un-printed labels (No PDF generated)"
    else  
      @specimens.each {|s| s.set_print_date()}
      respond_to do |format|
        format.html  
        format.pdf  do
            send_pdf_labels(@specimens, params[:label_type])
        end
      end
    end
  
  end
  
    
  def show
    @specimen = Specimen.find(params[:id])
    @site = @specimen.collection_event.site
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @specimen }
      format.pdf do
          send_pdf_labels([@specimen], params[:label_type])
      end
    end
  end
  

  
  # GET /specimens/new
  # GET /specimens/new.json
  def new
    if params[:collection_event_id]
      @collection_event = CollectionEvent.find(params[:collection_event_id])
      @site = @collection_event.site
      @specimen = @collection_event.specimens.build
    else
      @specimen = Specimen.new
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @specimen }
    end
  end

  # def duplicate
  #   if params[:specimen_id] && source_specimen = Specimen.find(params[:specimen_id])
  #     @specimen = Specimen.new(source_specimen.to_param)
  #   end
  # end
  
  # GET /specimens/1/edit
  def edit
    @specimen = Specimen.find(params[:id])
    @collection_event = @specimen.collection_event
  end

  # POST /specimens
  # POST /specimens.json
  def create
    @team = current_team
    @site = Site.find(params[:site_id])
    @collection_event = CollectionEvent.find(params[:collection_event_id])
    bulk_insert_count = params[:specimen][:bulk_insert_count].to_i
    bulk_insert_count = 1 unless (bulk_insert_count.is_a? Integer and bulk_insert_count > 0)
    
    @specimens = Specimen.bulk_new(bulk_insert_count, fix_collector_array(params[:specimen]))  # do bulk by default, even if just 1
    
    respond_to do |format|
      if Specimen.bulk_save(@specimens)
        format.html { redirect_to team_collection_event_path(@team,params[:collection_event_id]), notice:  "#{@specimens.count} specimen(s)  successfully created. " }
        format.json { render json: @specimen, status: :created, location: @specimen }
      else
        format.html { render action: "new" }
        format.json { render json: @specimen.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /specimens/1
  # PUT /specimens/1.json
  def update
    @specimen = Specimen.find(params[:id])
    
    respond_to do |format|

      specimen_params = fix_collector_array(params[:specimen])
      if @specimen.update_attributes(specimen_params)
        format.html { redirect_to team_specimens_path(current_team), notice: "Specimen #{@specimen.coded_id} was successfully updated." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @specimen.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /specimens/1
  # DELETE /specimens/1.json
  def destroy
    @specimen = Specimen.find(params[:id])
    @specimen.destroy

    respond_to do |format|
      format.html { redirect_to specimens_url }
      format.json { head :no_content }
    end
  end


  
  private
  
  # my current use of select2 and simple form creates an array with empty strings.  need to squash them for comma list
  # should probl do this in the model  
  def fix_collector_array(specimen_params)
    
    if specimen_params[:collectors].class == [].class
      # in place changes
      specimen_params[:collectors].delete("")
      specimen_params[:collectors].compact!
    
      # convert to string
      specimen_params[:collectors] = specimen_params[:collectors].join(",")
    end
    
    specimen_params
  end
  
end

