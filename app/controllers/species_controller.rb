class SpeciesController < ApplicationController
  load_and_authorize_resource :species
  
  def show
    @species = Species.find(params[:id])
  end
      
  def search
    @per_page = params[:per_page] || 20
    @page = params[:page] || 1
    
    if params[:q]
      @q = URI.unescape(params[:q])
      @species = Species.where_scientific_name_like(@q)
    end
  end
  
end
