class DashboardController < ApplicationController
  load_and_authorize_resource :team  
  # load_and_authorize_resource :sites, :through => :team
  # load_and_authorize_resource :collection_events, :through => :team
  
  def collect
    @sites = @team.sites
    @collection_events = @team.collection_events
    @specimens = @team.specimens
  end
end
