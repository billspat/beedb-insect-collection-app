class ApplicationController < ActionController::Base
  protect_from_forgery

  # before_filter :require_sign_in
  # before_filter :authenticate_user!

    
  def require_sign_in
    unless user_signed_in?
      flash[:error] = "This app requires log-in"
      redirect_to root_url
    else
      # do stuff for signed in people 
    end
  end
  
  def current_ability
    @current_ability ||= Ability.new(current_user)
  end
  
  def current_team
    @current_team || Team.find(params[:team_id]) || current_user.team
  end
  
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => exception.message
  end

end
