class UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
    authorize! :index, @user, :message => 'Not authorized as an administrator.'
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    authorize! :read, @user, :message => 'Not authorized as an administrator.'    
  end
  
  def edit
    @user = User.find(params[:id])
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
  end
  
  def update
    @user = User.find(params[:id])  
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    
    if @user.update_attributes(params[:user])
      redirect_to user_path(@user), :notice => "User updated."
    else
      redirect_to user_path(@user), :alert => "Unable to update user."
    end
  end
  
  def new
    @user = User.new()
    set_team!
    authorize! :update, @team, :message => "You are not authorized as a team manager"
    @user.team = @team
      
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @person }
    end    
  end
  
  def create
    # make user act like nested resource for team.  All users must have a team
    @user = User.new(params[:user])
    set_team!
    @user.team = @team
    
    authorize! :update, @team, :message => "You are not authorized to manage team #{@team.name}"
    authorize! :create, @user, :message => "You are not authorized to create users"
    
    respond_to do |format|    
      if @user.save
        msg = "A record for #{@user.name} was created in team #{@team.name}. "
        if params[:send_invite_flag] && @user.invitable?
            if @user.invite!
              msg << " An invitation was sent to #{@user.email} "
            end
        end
        
        format.html { redirect_to @team, notice: msg }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
    
  def destroy
    @user = User.find(params[:id])
    set_team!
    
    unless @user == current_user
      authorize! :destroy, @user, :message => 'Not authorized as an manager.'
      @user.destroy
      redirect_to team_path(@team), :notice => "User deleted."
    else
      redirect_to root_url, :notice => "Can't delete your own user account."
    end
  end
  
  def invite
    @user = User.find(params[:id])
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    
    if !@user || !@user.invitable?
      redirect_to user_path(@user), :alert => "Unable to invite user."
      return
    end
    
    if @user.invite!(current_user)
      redirect_to user_path(@user), :notice => "Invitation sent to #{@user.email}."
    else
      redirect_to user_path(@user), :alert => "Unable to send invite to user."
    end
 end
  
  private

  # get a team record from something, somehow!
  def set_team!
    unless @team
      @team = Team.find(params[:team_id]) if params[:team_id]
    end
  
    unless @team
        if @user && @user.team_id?
          @team = @user.team
        end
    end
    
    unless @team
      @team = current_user.team
    end
    
    @team
  end
       
    
end