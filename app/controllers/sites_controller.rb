class SitesController < ApplicationController
  load_and_authorize_resource :team
  load_and_authorize_resource :site, through: :team
  
  # # GET /sites
#   # GET /sites.json
  def index
    # @sites = Site.all
    # @mapjson = @sites.to_gmaps4rails
    @new_site = Site.new
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sites }
    end
  end
# 
#   # GET /sites/1
#   # GET /sites/1.json
  def show
    # @site = Site.find(params[:id])
    # @mapjson = @site.to_gmaps4rails
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @site }
    end
  end
# 
#   # GET /sites/new
#   # GET /sites/new.json
  def new
    @site = Site.new
    @site.active = true # this should go into the model
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @site }
    end
  end
# 
#   # GET /sites/1/edit
#   def edit
#     @site = Site.find(params[:id])
#   end
# 
  # POST /sites
  # POST /sites.json
  def create
    @site = Site.new(params[:site])
    @site.team_id = @team.id
    @site.active = true
    
    respond_to do |format|
      if @site.save
        format.html { redirect_to sitepath, notice: 'Site was successfully created.' }
        format.json { render json: @site, status: :created, location: @site }
      else
        format.html { render action: "new" }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /sites/1
  # PUT /sites/1.json
  def update
    # @site = Site.find(params[:id])

    respond_to do |format|
      if @site.update_attributes(params[:site])
        format.html { redirect_to sitepath, notice: 'Site was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end
# 
#   # DELETE /sites/1
#   # DELETE /sites/1.json
  def destroy
    @site = Site.find(params[:id])
   if @site.blank?
      redirect_to team_collect_path(current_team), :alert => "No site with this id found"
    end
    
    @team = @site.team
    begin
        @site.destroy

    rescue ActiveRecord::DeleteRestrictionError => e
        @site.errors.add(:base, e)

    ensure

      respond_to do |format|
        if @site.destroyed?
          format.html { redirect_to team_sites_path(@team), notice: "Site record #{@site.to_s} deleted. "  }
          format.json { head :no_content }
        else
          format.html { redirect_to team_site_path(@team, @site), 
                        alert: @site.errors.full_messages.map { |msg| p msg.to_s}.join("<br>") 
                    }
          format.json { render json: @site.errors, status: :unprocessable_entity  }
        end
      end
    end

  end
  
  
  protected
   
  def sitepath
     team_site_path(@site.team,@site)
    
  end
end
