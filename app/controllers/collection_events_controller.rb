class CollectionEventsController < ApplicationController
  
  include ControllersHelper
  
  load_and_authorize_resource :team  
  # load_and_authorize_resource :site   if params[:site_id] 
  # load_and_authorize_resource :collection_event #, :through => :site
  
  def index
    # @team = current_team
    if @site
      @collection_events = @collection_events.where('site_id= ?', params[:site_id])
    else
      @collection_events = @team.collection_events
    end
    
    respond_to do |format|
      format.html
        format.json { render json: @collection_events }
    end
    
  end
  
  def show
    @collection_event = CollectionEvent.find(params[:id])
    @site ||= @collection_event.site
      # /teams/:team_id/sites/:site_id/collection_events
      
    respond_to do |format|
      format.html
      format.json { render json: @collection_event }
    end
  end
  
  def edit
    @collection_event = CollectionEvent.find(params[:id])
    @site = Site.find(params[:site_id] || @collection_event.site_id)
  end
  
  def new
    @site=  Site.find(params[:site_id]) 
    @team = @site.team
    @collection_event = @site.collection_events.build
    
    respond_to do |format|
      format.html
      format.json { render json: @collection_event }
    end
    
  end

  def create
    convert_date_params([:date_start, :date_end])
    
    site_id = params[:site_id] || params[:collection_event][:site_id]
    ce_params = fix_collector_array(params[:collection_event])    
    

    if site_id
      @site = Site.find(site_id)
      @collection_event = @site.collection_events.build(ce_params)
    else
      redirect_to sites, notice: "Please select a site before creating an event"
      @collection_event = CollectionEvent.new
    end
    
    @team = ( @site.team if @site ) || current_team
  
    respond_to do |format|
      if @collection_event.save
        format.html { redirect_to eventpath, notice: 'Event was successfully created.' }
        format.json { render json: @collection_event, status: :created, location: @collection_event }
      else
        format.html { render action: "new" } 
          #redirect_to new_team_site_collection_event_path(@team,@site,@collection_event), notice: "Error creating event" }
        format.json { render json: @collection_event.errors, status: :unprocessable_entity }
      end

   end
  end


  def update
    convert_date_params([:date_start, :date_end])
    
    @collection_event = CollectionEvent.find(params[:id])
    @site = @collection_event.site unless @site
    ce_params = fix_collector_array(params[:collection_event])
    
    respond_to do |format|
        if @collection_event.update_attributes(ce_params)
          format.html { redirect_to team_site_collection_event_path(@team, @site, @collection_event), notice: "Collection event #{@collection_event.to_s} was successfully updated." }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @collection_event.errors, status: :unprocessable_entity }
        end
    end
  end
  
  def destroy
    @collection_event = CollectionEvent.find(params[:id])  
    
    if @collection_event.blank?
      redirect_to team_collect_path(current_team), :alert => "no collection event with this id"
    end
    
    @site = @collection_event.site
    @team = @site.team
    begin
        @collection_event.destroy
    rescue ActiveRecord::DeleteRestrictionError => e
        @collection_event.errors.add(:base, e)
    ensure
      respond_to do |format|
        if @collection_event.destroyed?
          format.html { redirect_to team_site_path(@team, @site), notice: "Collection Event #{@collection_event.to_s} deleted. "  }
          format.json { head :no_content }
        else
          format.html { redirect_to team_site_collection_event_path(@team, @site, @collection_event) , 
                        alert: @collection_event.errors.full_messages.map { |msg| p msg.to_s}.join("<br>") 
                      }
          # TO DO : determine which kind of alert is better here when deletion fails    
          # format.html { redirect_to team_site_path(@team, @site), notice: "Collection Event #{@collection_event.to_s} could not be deleted. "  }
                      
          format.json { render json: @collection_event.errors, status: :unprocessable_entity  }
         end
      
      end
    end
  end
  
  protected

  def eventpath
     team_collection_event_path(@team,@collection_event)
  end
  
  def convert_date_params(date_params = [])
    date_params.each do |date_param|
      params[:collection_event][date_param] = params[:collection_event][date_param].to_date unless params[:collection_event][date_param].blank? 

     # params[:collection_event][date_param] = p.to_date unless p.blank? 
    end
  end
  
  
end
