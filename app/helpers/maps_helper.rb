module MapsHelper
  
  # string generators/helpers for google staic maps  
  
  # no longer used, gmap code split into two
  def get_coordinates(spatial_resource, get_from_ip = true)
    if (!spatial_resource.latitude? || !spatial_resource.longitude?) and get_from_ip
      # get the lat long from current IP address
      # use geoplugin in production, but here just return a static lat lng
      default_coordinates
    else    
      "#{spatial_resource.latitude},#{spatial_resource.longitude}"
    end
    
  end
  
  def default_coordinates()
    msu = "42.721780763615,-84.474603831768"
    center_us = "42.488302, -98.613281"
  end
  
  def gmap_markers(collection, color="red")
    delim = "|" # "%7C"
    points = collection.collect{|s| coordinates(s) }
    "color:#{color}#{delim}#{points.join(delim)}".html_safe
  end
  
  
  def static_map(sites, options = {})
    
    # let google determine the default
    # default_center = .coordinates(sites.first)
    
    # some defaults to make calling this very easy
    options = { :size => "300x300",
        :maptype => "hybrid",
        :markers => gmap_markers(sites),
        :sensor  => "false",
        :zoom    => "13" }.merge(options)
    
    src_url =  "http://maps.google.com/maps/api/staticmap"
  
    image_tag(src_url + "?" + options.to_query.html_safe, {:size => options[:size], :alt => "Map of Site(s)"}) 
  end
  
  def google_key
    ENV['GOOGLEKEY']
  end
  
  def gmap_src 
    "http://maps.googleapis.com/maps/api/js?key=#{google_key}&sensor=false"
  end
  
  def google_map(sites, options = {})
    content_for(:head) do 
      javascript_include_tag gmap_src
    end
  end

  def gmap_asyncy(sites, options={})
    
   
  end
    
  
end
