module UsersHelper

  def user_hash(user)
    {                                                              
      "Name" => user.name, 
      "Position" => user.position,                                   
      "email" => user.email    ,                                     
      "Affiliation" => user.affiliation,                             
      "Contact"=> user.contact         ,                             
      "Has database account?" => account_yes_no(user), 
      "Invitation?" => invitation_status(user),  
      "Roles" => user.roles_name.join(",") ,  
      "Team" => (user.team.name if user.team ), 
      "Notes" => (user.notes)
    }
    
  end
  
  def invitation_status(user)
    if user.invitation_accepted_at?
      "Accepted #{user.invitation_accepted_at.to_date.to_s}"
    elsif user.invitation_sent_at?
      "Sent #{user.invitation_sent_at.to_date.to_s}"
    else
      "Not Invited"
    end
  end
  
  def account_yes_no(user)
    (user.registered? ? "Yes" : "No")
  end
  

end
