module ControllersHelper
  # miscellaneous helpers
  
  # used where collectors are entered in events and/or specimens
  def fix_collector_array(model_params)
    
    if model_params[:collectors].class == [].class
      # in place changes
       model_params[:collectors].delete("")
       model_params[:collectors].compact!
       model_params[:collectors] =  model_params[:collectors].join(",")
    end
    
     model_params
  end
end