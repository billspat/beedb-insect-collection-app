module ApplicationHelper
  
  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  
    
  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  
  # layout helpers
  
  def title(title_text)
    content_for(:title) do
      title_text
    end
  end
  
  # bootstrap helpers
  def disable_navbar
    css_content = "div.navbar{display:none !important;}"
    content_tag :style, css_content, {:type=>"text/css", :media=>"all"}
  end

  
  # BOOTSTRAP BUTTONS!
  
  def iconified_link_to(icon, body, url, html_options = {})
    # not a button, see below
    link_to url, html_options do
      "#{content_tag(:i, nil, :class => icon.to_s )} &nbsp;#{body}".html_safe
    end
  end
  
  def cancel_link_to(url = :back, body = "Cancel", html_options = {})
    icon =  'icon-remove'
    iconified_link_to(icon, body, url, html_options)
  end
  
  def delete_link_to(url, html_options={})
    html_options[:class] = (html_options[:class] || " text-danger" )
    html_options[:method] = :delete
    html_options[:data] = { :confirm => 'Are you sure?' }
    iconified_link_to('icon-trash', "Delete", url, html_options)
  end
  
  def iconified_button_to(icon, body, url, html_options = {})
    html_options[:class] = (html_options[:class] || "" ) + ' btn' #add default btn class to other classes
    iconified_link_to(icon, body, url, html_options).html_safe
  end
  
  def edit_button(url, html_options = {}, button_text="Edit")
    iconified_button_to('icon-pencil', button_text, url, html_options)
  end

  def delete_button(url, html_options = {})
    html_options[:class] = (html_options[:class] || " btn btn-danger" )
    html_options[:method] = :delete
    html_options[:data] = { :confirm => 'Are you sure?' }
    iconified_link_to('icon-trash icon-white', "Delete", url, html_options)
  end
  
  def cancel_button(url = nil, html_options = {})
     html_options[:class] = (html_options[:class] || "" ) + ' btn-warning'
     iconified_button_to('icon-remove icon-white', "Cancel", url || :back , html_options)
  end
  
  # generic model display  helpers
    
  def to_query(hash)
    hash.map{|k,v| "#{k}=#{v}"}.join('&')
  end
  
  def hash_to_rows(hash)
    rows = hash.collect do |k,v| 
      content_tag(:tr) do 
        "<td>#{k}&nbsp;</td><td><strong>#{v}</strong></td>".html_safe
      end
    end
    content_tag(:tbody, rows.join("\n").html_safe)
  end
  
  def hash_table(hash)
    content_tag(:table, :class=>"table table-hover") do 
      hash_to_rows(hash).html_safe
    end  
  end
  
  def yes_no(bool)
    bool ? "yes" : "no"
  end


end
