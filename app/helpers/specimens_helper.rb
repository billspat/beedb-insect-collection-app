require 'csv'
require "open-uri"
require "specimen_exporter"
require "specimen_label"

module SpecimensHelper
  
  def specimen_nested_path(specimen = @specimen)
    team_site_collection_event_specimen_path(@team, specimen.collection_event.site, specimen.collection_event, specimen)
  end
 
  # def specimens_to_xls(specimens)
  #   SpecimenExporter.to_html(specimens)
  # end

  # superceded by lib/specimen_exporter.rb, remove in future commit
  # def specimen_export(specimens)
  #   if specimens.blank?
  #     return ("No specimens selected")
  #   end
  #
  #   csv_options = { :col_sep => ",",
  #                   :headers => Specimen.export_header,
  #                   :write_headers => true }
  #
  #   CSV.generate(csv_options) do |csv|
  #      specimens.each do |specimen|
  #        csv << specimen.export_data
  #      end
  #    end
  # end

  def send_pdf_labels(specimens, label_type)
      # label type from params here
      send_data SpecimenLabel.generate_pdf(specimens, label_type),  # TO DO : CHECK LABEL_TYPE PARAM
          filename: label_file_name(),
          stream: false,
          type: "application/pdf",
          disposition: "inline" 
  end
  
  def temp_pdf_file  
    "/tmp/" + label_file_name
  end
    
  def label_file_name
    timestamp = Time.now.utc.iso8601.gsub('-', '').gsub(':', '')
    "specimens_printed_#{timestamp}.pdf"
  end
    
  
end
