module CollectionEventHelper

  # these methods help have the calendar show a relevant month
  # either the month in params, if one or the month with the last event
  
  def date_field(f,attribute,placeholder)
    f.input_field(attribute,
        :placeholder => placeholder,
        :style =>"width:120px;",
        :as => :string
        )    
  end
  
  def collector_hash
    team = @team || current_team
    Hash[@team.users.all.map{|u| [u.collector_name, u.collector_name]}]
  end
    
    
  def year_to_display(collection_events)
    if params[:year].blank?
      events_latest_year(collection_events) unless collection_events.blank?
    else
      params[:year].to_i      
    end
  end

  def month_to_display(collection_events)
    if params[:month].blank?
      events_latest_month(collection_events) unless collection_events.blank?
    else
      params[:month].to_i      
    end
  end
  
  def events_latest(collection_events)
    collection_events.scoped.maximum('date_start') unless collection_events.blank?
  end
  
  def events_latest_year(collection_events) # relation
    events_latest(collection_events).year unless collection_events.blank?
  end

  def events_latest_month(collection_events) # relation
    events_latest(collection_events).month unless collection_events.blank?
  end
  
  
end
