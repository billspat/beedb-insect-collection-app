module TeamsHelper
  
  def team_hash(team)
    {
      "Name" => team.name, 
      "Affiliation" => team.affiliation,
      "Notes" => team.notes
    }
  end
  
end
