require 'test_helper'

class SpecimensControllerTest < ActionController::TestCase
  setup do
    @specimen = specimens(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:specimens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create specimen" do
    assert_difference('Specimen.count') do
      post :create, specimen: { collection_event_id: @specimen.collection_event_id, collectors: @specimen.collectors, determination_status: @specimen.determination_status, determiner: @specimen.determiner, hostplant_id: @specimen.hostplant_id, hostplant_literal: @specimen.hostplant_literal, idcode: @specimen.idcode, label_printed_on: @specimen.label_printed_on, location: @specimen.location, notes: @specimen.notes, status: @specimen.status, team_id: @specimen.team_id }
    end

    assert_redirected_to specimen_path(assigns(:specimen))
  end

  test "should show specimen" do
    get :show, id: @specimen
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @specimen
    assert_response :success
  end

  test "should update specimen" do
    put :update, id: @specimen, specimen: { collection_event_id: @specimen.collection_event_id, collectors: @specimen.collectors, determination_status: @specimen.determination_status, determiner: @specimen.determiner, hostplant_id: @specimen.hostplant_id, hostplant_literal: @specimen.hostplant_literal, idcode: @specimen.idcode, label_printed_on: @specimen.label_printed_on, location: @specimen.location, notes: @specimen.notes, status: @specimen.status, team_id: @specimen.team_id }
    assert_redirected_to specimen_path(assigns(:specimen))
  end

  test "should destroy specimen" do
    assert_difference('Specimen.count', -1) do
      delete :destroy, id: @specimen
    end

    assert_redirected_to specimens_path
  end
end
