# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hostplant do
    name "MyString"
    description "MyText"
    taxon_name "MyString"
    taxon "MyString"
  end
end
