# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :collection_location, :class => 'CollectionLocation' do
    year "MyString"
    location "MyString"
    description "MyString"
    value 1
  end
end
