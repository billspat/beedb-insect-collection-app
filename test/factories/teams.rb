# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :team do
    name "MyString"
    affiliation "MyString"
    url "MyString"
    notes "MyText"
  end
end
