# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :species do
    taxonomic_order ""
    genus "MyString"
    species "MyString"
    sub_species "MyString"
    common_name "MyString"
    common_synonyms "MyString"
    notes "MyText"
  end
end
