# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :treatment do
    name "MyString"
    treatment_code "MyString"
    year 1
    active false
    notes "MyText"
  end
end
