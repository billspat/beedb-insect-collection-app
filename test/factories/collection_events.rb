# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :collection_event do
    site 1
    team 1
    date_start "2013-04-25"
    date_end "2013-04-25"
    distance_from_zero 1
    collectors "MyString"
    collection_time "2013-04-25 16:40:20"
    lat 1
    long 1
    date_verbatim 1
    collection_type_id 1
    collection_type_text "MyString"
  end
end
