# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :specimen do
    idcode "MyString"
    collection_event_id 1
    team_id 1
    hostplant_id 1
    hostplant_literal "MyString"
    determination_status "MyString"
    determiner "MyString"
    collectors "MyString"
    label_printed_on "2013-05-13"
    status "MyString"
    location "MyString"
    notes "MyText"
  end
end
