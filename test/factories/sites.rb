# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :site do
    team_id 1
    location_id 1
    lat 1.5
    long 1.5
    elevation 1.5
    name "MyString"
    treatment "MyString"
    active false
    note "MyText"
  end
end
