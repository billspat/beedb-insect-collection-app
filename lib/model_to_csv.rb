
# 
module ModelToCSV
  
  module ClassMethods
    def records_to_csv(records, export_options = {}, csv_options = {})  
      return nil if records.blank?
      
      # use default CSV options or those passed in
      header_from_hash = records[0].to_hash.keys # need a more elegant way to do this outside of this method...     
      csv_options.merge!(default_csv_options(header_from_hash)) 
      
      csv = records.map {|m| m.to_hash(export_options).values }.to_csv(csv_options)
        
    end
    
    def default_csv_options(csv_header = nil)
      default_csv_options = {:col_sep =>  "\t", :write_headers => true, headers => csv_header }
      
    end
    
  end
  
  module InstanceMethods
    
    def to_hash(export_options = nil)
      self.attributes
    end
    
    
  end
  
  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end

end
