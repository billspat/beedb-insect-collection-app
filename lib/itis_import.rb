# Species schema
#     string   "taxonomic_family"
#     string   "taxonomic_order"
#     string   "genus"
#     string   "species"
#     string   "sub_species"
#     string   "common_name"
#     string   "common_synonyms"
#     text     "notes"
#     string   "tempname"
#     string   "superfamily"
#     string   "subfamily"
#     string   "tribe"
#     string   "itis_tsn",
#     string   "taxon_author"

# a mixin to add toto import a CSV file with a header from ITIS

class ItisImport
  # add method to Species model for this purpose
  # Itis Species columns : Superfamily,Family,Subfamily,Tribe,Scientific Name,Taxon Author,tsn,Credibility Rating,usage,update_date,Incertae Sedis indicator

  def self.test
    "hello from ItisImport"
  end
  
  # skip duplicated genus and species... 
  def self.import(csvfile)
    Species.transaction do
      CSV.foreach(csvfile, {:headers => true, :header_converters => :symbol, :converters => :all}) do |row|  
        # convert the row to a hash acceptable to the data model, you must define that method
        h = self.itis2attributes(row)
        unless self.duplicated(h)
          rec = Species.new(h)
          rec.save
        end      
      end
    end # end transaction
  end  
  
  # this should be put into the model itself
  def self.itis2attributes(row)
    
    # header Family, 
    bee_order = 'Hymenoptera'
    superfamily = row[:superfamily]
    taxonomic_family = row[:family]
    subfamily = row[:subfamily]
    tribe = row[:tribe]
    genus,species = row[:scientific_name].split(/\s+/) # split on any number of spaces
    taxon_author = row[:taxon_author]
    notes = "tsn=" + row[:tsn].to_s
  
    { :taxonomic_order => bee_order,
      :superfamily => superfamily, 
      :taxonomic_family => taxonomic_family, 
      :subfamily=> subfamily,
      :tribe => tribe, 
      :genus => genus, 
      :species => species,
      :taxon_author => taxon_author, 
      :itis_tsn => row[:tsn],
      :notes => notes
    }    
  
  end
  
  def self.duplicated(s)
    !Species.where("genus = ? and species = ?", s[:genus], s[:species]).empty?
  end
      

end # ItisImport class

# class Plants
#   # include ItisImport
# 
#   def self.itiscsv2hash(row)
#     # add code specific to plant 
#   end
# end

# plants list db
# "Symbol","Synonym Symbol","Scientific Name with Author","Common Name","Family"
# symbol = alpha ID code 
