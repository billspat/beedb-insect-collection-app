class LocShortener
  @@shortening_file = "#{Rails.root}/config/shortening.yaml"
  @@shortenings = {}
  yamlstr = File.read(@@shortening_file)
  YAML.load(yamlstr).each {|k,v| @@shortenings[Regexp.new(k,Regexp::IGNORECASE)] = v}
  
  class << self
  
    def shortenings
      @@shortenings
    end
    
    def shorten!(str)
      str.strip!
      @@shortenings.each {|pattern, shortstr| str.gsub!(pattern,shortstr)}
      str
    end 
    
    def shorten(str)
     s =  str.dup
     shorten!(s)
   end
   
  end
end
