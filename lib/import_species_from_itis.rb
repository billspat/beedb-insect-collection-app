# very specific way of importing species -------
class ImportSpeciesFromItis
def initialize(csvfile)
  @csvfile = csvfile
end

def import
  rec_count = 0
  sp_records = Species.all.count
  Species.transaction do
    CSV.foreach(@csvfile, {:headers => true, :converters => [:symbol]}) do |row|  
      sp = Species.new(row2hash(row))
      sp.save
      rec_count += 1
    
    end
  end # end transaction

  p "#{rec_count} records imported, #{sp_records} starting, now #{Species.all.count} records in db table"
  # handle failure...?
end


def row2hash(itsrow)
    # header Family, 
    bee_order = 'Hymenoptera'
    taxonomic_family = row[:family]
    genus,species = row[:scientific_name].split(/\s*/) # split on any number of spaces
    notes = "tsn=" + row[:tsn]
    {:taxononomic_order => bee_order, :taxonomic_family => taxonomic_family, :genus => genus, :species => species, :notes => notes}
end

end