module DebugTest
    def whoAmI?
      "#{self.type.name} (\##{self.id}): #{self.to_s}"
    end
end