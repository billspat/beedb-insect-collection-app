class SpecimenExporter
#  custom data view methods for CSV cells, called on single resource (row)


  def initialize(specimen)
    @specimen = specimen
    @collection_event = @specimen.collection_event
    @site = @specimen.site
  end

  def export_data 
      {
      "Specimen ID Code"      =>@specimen.coded_id,
      "Collection Site"       =>@site.name,
      "State/Prov"            =>@site.stateprov,
      "Country"               =>@site.country,
      "Treatment"             =>@site.treatment.to_s,
      "Collection on"         =>@collection_event.date_start,
      "Collection end date"   =>@collection_event.date_end,
      "Collection time"       =>(@collection_event.collection_time.strftime('%h:%m') unless @collection_event.collection_time.blank?),
      "Collected by"          =>@specimen.collectors,
      "Host plant"            =>self.hostplant_str,
      "Species"               =>@specimen.scientific_name_str,
      "Sex"                   =>@specimen.sex,
      "Specimen Status"       =>@specimen.status,
      "Location"              =>@specimen.location,
      "Determination status"  =>@specimen.determination_status,
      "Determiner"            =>@specimen.determiner,
      "Notes"                 =>@specimen.notes
     }

  end
  
  def export_header
    export_data.keys
  end
  
  def hostplant_str
    if @specimen.hostplant_id?
      @specimen.hostplant.to_s
    else
      @specimen.hostplant_literal
    end
  end


  class << self
  
    def data_headers(specimens)
      self.new(specimens[0]).export_data.keys  unless specimens.blank?
    end
    
    def to_csv(specimens, csv_options = {}) 
      return("") if specimens.blank?

      exporters = specimens.map {|specimen| SpecimenExporter.new(specimen) }       
       #  the fact that the export header is gen'd by an object instance doesn't smell right
       #  but since I don't use a decent dsl here, that's what works
       
      @csv_options = {:col_sep =>  "\t",
                      :headers => self.data_headers(specimens),
                      :write_headers => true }                    

      @csv_options.merge!(csv_options) 
              
      @csv = CSV.generate(@csv_options) do |csv| 
         exporters.each do |exporter|
           csv << exporter.export_data.values
         end
      end
      exporters = nil
      @csv
    end
    
    
    def to_xlsx(specimens)
      @specimens = specimens
      @headers = self.data_headers(specimens)
      xlsx_str = ERB.new(self.xlsx_template).result(binding)
    end
    
    def to_html(specimens)
      @specimens = specimens
      @headers = self.data_headers(specimens)
      html_str = ERB.new(self.html_template).result(binding)      
    end
    
    def xlsx_template
<<XLSX
<?xml version="1.0"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:html="http://www.w3.org/TR/REC-html40">
  <Worksheet ss:Name="Sheet1">
    <Table>
      <Row>
        <% for header in @headers %>
          <Cell><Data ss:Type="String"><%= header %></Data></Cell>
        <% end %>      
      </Row>
      <% for specimen in @specimens %>
      <Row>
        <% @values = SpecimenExporter.new(specimen).export_data.values %>
        <% for value in @values %>
           <Cell><Data ss:Type="String"><%= value %></Data></Cell>
        <% end %>
      </Row>
      <% end %>
    </Table>
  </Worksheet>
</Workbook>      
XLSX
    
    end
    
    def html_template
<<HTMLTABLE
  <h1>Specimen Data from ICP Bee Database</h1>

  <table>
    <tr>
    <% for header in @headers %>
      <th><%= header %></th>
    <% end %>
    </tr>

    <% for specimen in @specimens %>
      <tr>
      <% @values = SpecimenExporter.new(specimen).export_data.values %>
      <% for value in @values %>
        <td><%= value %></td>
      <% end %>  
      </tr>
    <% end %>
  </table>

  <br />
HTMLTABLE
  end
  
  
end # class methods 


end
