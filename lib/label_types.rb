require 'yaml'

module LabelTypes
    extend self
    
    @@label_types_file = 'config/label_types.yml'
    @@label_types = YAML.load(File.open(@@label_types_file))
    
    def get_type(name)
        @@label_types[name]
    end
    
    def [](name = nil)
        name ||= 'default'
        get_type(name) if @@label_types.has_key?(name)
    end

    def default
        get_type('default')
    end

    def list_types
        @@label_types.keys
    end
    
    def has_type?(t)
        @@label_types.has_key?(t)
    end
    
end

