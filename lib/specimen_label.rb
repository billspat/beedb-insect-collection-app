
# specimen_label.rb
# class to manage rendering of a single label with Prawn::Labels gem that have a specialized label paper spec, 2-d barcode, and processed/shortened text
# render entire PDF in a helper that uses this class as a delegator for the model 
# this ideally should be abstracted into a label_with_barcode class used by Prawn::labels gem

# example loop using Prawn labels in a Helper class
 
# given an array of specimens, and a known type
#
#   

# barby is bar code generator
require 'barby'
require 'barby/outputter/prawn_outputter'
require 'barby/barcode/data_matrix'
 
class SpecimenLabel

  # label_types is hash of hashes keyed on name of type, stored in YAML
  @@label_types = YAML.load(File.open(ENV['LABEL_TYPES_FILE']))
  @@label_type_name = 'default'  
  
  attr_reader :barcode
    
  def initialize(specimen)
    @specimen    = specimen
    @barcode     = datamatrix(@specimen.coded_id)
    @code_size   = label_conf :code_size 
    @font_size   = label_conf :font_size 
    @code_margin = label_conf :code_margin    
  end
  
  def label_conf(v)
    @@label_types[@@label_type_name][v.to_s] if v
  end
  
  def datamatrix(value)
    Barby::PrawnOutputter.new(Barby::DataMatrix.new(value))
  end
  
  # render a single label in the pdf doc's current bounding box 
  # perhaps move this to an ERB template single string seperated by br and render(:partial "specimens/_labels.html.erb") 
  def render(doc)
    # get current bounding_boxm, where Prawn::Labels says the next label should be
    bb = doc.bounds
    
    # add barcode inside this bb, but use Prawn float to reset the cursor
    doc.float do   
      barcode.annotate_pdf(doc,:margin=>@code_margin, :x => bb.width - @code_size - @code_margin , :y => bb.height - @code_size -2 ,:height=>@code_size )        
        # :align =>:right, :valign => :center
        
      doc.draw_text @specimen.coded_id, :size=>@font_size, :rotate =>90, :at =>[bb.width, @font_size+1], :style=> :bold 
        # bb.height-@specimen.coded_id.length*@font_size
    end
    
    # add the label's text via local to_s method
    doc.text_box self.to_s, :size=>@font_size, :width => (bb.width - @code_size - @code_margin), :height=> bb.height 
  end
  
  def id
    # TO DO: get this from label_type config...      
    @specimen.coded_id
  end
  
  def to_s
    s = label_text.join("\n")  # local label_text...
    LocShortener.shorten!(s)
    # IF I was clever enough to use an ERB template, here is where I'd do it to convert a model into text
    # render_to_string(:layout => false, :template => "specimens/label.html.erb", :locals => {:site => @site, :specimen => @specimen})
  end

  # Prawn labels gem, if using the fit to size feature, assumes the class int he block is string and has an obj.split method
  # implement it here so prawn labels will work
  def split(pattern)
    self.to_s.split(pattern)
  end

  def label_text
      collection_event = @specimen.collection_event
      site = collection_event.site 
         
      [ @specimen.shortened_geoname.to_s,
        @specimen.shortened_locality.to_s,
         "#{site.coordinates(4)}  #{collection_event.short_date}",
         "#{@specimen.shortened_collectors} trt:#{@specimen.shortened_location(30-@specimen.shortened_collectors.length)}",
         "#{@specimen.host_plant_str}"
      ]
  end
  
  class << self
  
        def get_label_type()
            @@label_type_name
        end
    
        def default_label_type()
            @@label_types['default']
        end
 
        def set_label_type(type_name = nil)
            if type_name and @@label_types.has_key?(type_name)
                @@label_type_name  = type_name 
            else
                @@label_type_name = "default"  # to do ; exception here if type name not in config
            end
            Prawn::Labels.types = {@@label_type_name => @@label_types[@@label_type_name] }
        end
        
        # def add_label_type(type_name, type_config_hash)
        #     # given a hash, adds to class array
        # end
        #           

        def generate_pdf(specimens, label_type_name)
          self.set_label_type(label_type_name) # TO DO : make sure this if valid type or throw
  
          if specimens.blank?
              generate_blank(" ")
              return
          end
  
          speclabels = specimens.map{|s| SpecimenLabel.new(s)}
          Prawn::Labels.render(speclabels,:type=>@@label_type_name) do |pdf, speclabel|
            speclabel.render(pdf)
          end
        end

        def generate_blank(message=nil)
          pdf = Prawn::Document.new
          pdf.text ( message || " " ) 
          pdf.render
        end
        
        # used by specimen_controller 
        # def single_label(specimen)
        #   pdf = Prawn::Document.new
        #   pdf.text specimen.label_text_lines.join("\n")
        #   pdf
        # end
        
  end # class<<self
  
    
end  

