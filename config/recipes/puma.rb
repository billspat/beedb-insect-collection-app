set_default(:puma_pid) { "#{current_path}/tmp/pids/puma.pid" }
set_default(:puma_log) { "#{shared_path}/log/puma.log" }

namespace :puma do
  desc "Setup #{application} initializer and app configuration"
  task :setup, roles: :app do
    run "mkdir -p #{shared_path}/config"
    run "mkdir -p #{shared_path}/sockets"
    run "touch #{shared_path}/sockets/#{application}.socket"
    template "puma.rb.erb", "#{shared_path}/config/puma.rb"
    template "puma_init.erb", "#{shared_path}/config/#{application}_init.sh"
    run "chmod +x #{shared_path}/config/#{application}_init.sh"
    run "#{sudo} ln -nfs #{shared_path}/config/#{application}_init.sh /etc/init.d/#{application}"
    run "#{sudo} update-rc.d -f #{application} defaults"
  end
  after "deploy:setup", "puma:setup"

  %w[start stop restart].each do |command|
    desc "#{command} #{application}"
    task command, roles: :app do
      run "#{sudo} service #{application} #{command}"
    end
    after "deploy:#{command}", "puma:#{command}"
  end
end