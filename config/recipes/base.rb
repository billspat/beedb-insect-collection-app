def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

namespace :deploy do
  desc "Install everything onto the server"
  task :install do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install python python-software-properties python-pycurl git libct4 libsybdb5 curl build-essential gcc libfontconfig bash patch bzip2 openssl libreadline6 libreadline6-dev zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev autoconf libc6-dev libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev dkms"
    run "#{sudo} apt-get -y dist-upgrade"
  end

  desc "Move database.example.yml file to database.yml in shared location"
  task :setup_database_config do
    run "mkdir -p #{shared_path}/config/"
    put File.read("config/database.example.yml"), "#{shared_path}/config/database.yml"
    puts ""
    puts "------------------------------------------------------------------------"
    puts "----------------------------- IMPORTANT --------------------------------"
    puts "------------------------------------------------------------------------"
    puts "Now edit the database config file in #{shared_path}/config/database.yml."
    puts ""
    puts ""
  end
  after "deploy:setup", "deploy:setup_database_config"

  desc "Test Capistrano task"
  task :greeting do
    puts "Are you ready to deploy #{application}"
  end

  desc "Configure server firewall"
  task :configure_firewall do
    run "#{sudo} ufw allow 80"
    run "#{sudo} ufw allow 443"
    run "#{sudo} ufw allow #{port}"
    run "#{sudo} ufw default deny"
    run "#{sudo} ufw --force enable"
  end
  after "deploy:install", "deploy:configure_firewall"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

end