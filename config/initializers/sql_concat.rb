# directly from 
# http://stackoverflow.com/questions/2986405/database-independant-sql-string-concatenation-in-rails#12514997
 
module ActiveRecord
  module ConnectionAdapters
    class AbstractAdapter
 
      # Will return the given strings as a SQL concationation. By default
      # uses the SQL-92 syntax:
      #
      #   concat('foo', 'bar') -> "foo || bar"
      def concat(*args)
        args * " || "
      end
 
    end
 
    class AbstractMysqlAdapter < AbstractAdapter
 
      # Will return the given strings as a SQL concationation.
      # Uses MySQL format:
      #
      #   concat('foo', 'bar')  -> "CONCAT(foo, bar)"
      def concat(*args)
        "CONCAT(#{args * ', '})"
      end
 
    end
 
    class SQLServerAdapter < AbstractAdapter
 
      # Will return the given strings as a SQL concationation.
      # Uses MS-SQL format:
      #
      #   concat('foo', 'bar')  -> foo + bar
      def concat(*args)
        args * ' + '
      end
 
    end
  end
end