require "bundler/capistrano"

server "35.9.119.25", :web, :app, :db, primary: true
set :server_name, "icp.entomlogy.msu.edu"
set :port, 22
set :user, "bdbapp"
set :application, "bdb"
set :deploy_to, "/home/#{user}/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :scm, "git"
set :ruby_version, "1.9.3-p448"
set :repository, "git@gitlab.msu.edu:billspat/beedb-insect-collection-app.git"
set :branch, "master"

load "config/recipes/base"
load "config/recipes/rbenv"
load "config/recipes/nodejs"
load "config/recipes/nginx"
load "config/recipes/puma"
load "config/recipes/check"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases

set :default_environment, {
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}
