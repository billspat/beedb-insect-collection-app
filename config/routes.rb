Bdb::Application.routes.draw do

  # resources :species, :only => [:show], :defaults => { :format => 'json' }
  get 'species/:id', to: 'species#show', defaults: { format: 'json' }
  get "species", to: 'species#search', defaults: {format: 'json'}

  
  authenticated :user do
    root :to => 'home#index'
  end

  root :to => "home#index"
  devise_for :users,  :skip => [:registrations]  #:controllers => { :invitations => "invitations" },
  
  resources :specimens, :only => [:index, :show, :edit]
  # printing a specimen has a side effect of updating printed date, so it's a put not a get
  
  
   
  resources :teams do 
    # dashboard page
    get 'collect' => 'dashboard#collect'
             
    
    resources :managers
    
    resources :collection_events do
      resource :specimens, :only => [:new, :create]
    end
    # , :except => [:new, :create]
    resources :specimens, :only => [:index, :show, :edit, :update, :destroy] do 
      collection do
        put "print", to: 'specimens#print_labels',  defaults: { format: 'pdf'}
      end
    end
    
    
    resources :sites do 
      resources :collection_events do
        resources :specimens
      end
    end
    
  end
  
  as :user do
    get 'users/registration' => 'devise/registrations#edit', :as => 'edit_user_registration'    
    put 'users/registration' => 'devise/registrations#update', :as => 'user_registration' 
    delete 'users/registration' =>  'devise/registrations#delete', :as => 'delete_user_registration'          
  end 
  
  resources :users do 
    member do
       put 'invite'
     end
  end
   
  
end