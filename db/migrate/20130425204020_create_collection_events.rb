class CreateCollectionEvents < ActiveRecord::Migration
  def change
    create_table :collection_events do |t|
      t.references :site
      t.references :team
      t.date :date_start
      t.date :date_end
      t.integer :distance_from_zero
      t.string :collectors
      t.time :collection_time
      t.integer :lat
      t.integer :long
      t.integer :date_verbatim
      t.integer :collection_type_id
      t.string :collection_type_text

      t.timestamps
    end
    add_index :collection_events, :site_id
    add_index :collection_events, :team_id
  end
end
