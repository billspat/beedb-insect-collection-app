class AddSuperFamilyToSpecies < ActiveRecord::Migration
  def change
    add_column :species, :superfamily, :string
    add_column :species, :subfamily, :string
    add_column :species, :tribe, :string
    add_column :species, :itis_tsn, :string
    add_column :species, :taxon_author, :string
  end
end
