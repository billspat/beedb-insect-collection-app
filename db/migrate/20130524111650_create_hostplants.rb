class CreateHostplants < ActiveRecord::Migration
  def change
    create_table :hostplants do |t|
      t.string :name
      t.text :description
      t.string :taxon_name
      t.string :taxon

      t.timestamps
    end
  end
end
