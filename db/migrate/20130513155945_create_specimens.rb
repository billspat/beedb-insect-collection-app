class CreateSpecimens < ActiveRecord::Migration
  def change
    create_table :specimens do |t|
      t.string :idcode
      t.integer :collection_event_id
      t.integer :team_id
      t.integer :hostplant_id
      t.string :hostplant_literal
      t.string :determination_status
      t.string :determiner
      t.string :collectors
      t.date :label_printed_on
      t.string :status
      t.string :location
      t.text :notes

      t.timestamps
    end
  end
end
