class DropTreatmentFromSite < ActiveRecord::Migration
  def up
    remove_column :sites, :treatment
  end

  def down
    add_column :sites, :treatment, :string
  end
 
end
