class AddLocalityToSite < ActiveRecord::Migration
  def change
    add_column :sites, :region, :string
    add_column :sites, :city, :string
    add_column :sites, :county, :string
    add_column :sites, :stateprov, :string
    add_column :sites, :postalcode, :string
  end
end
