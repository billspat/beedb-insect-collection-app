class CreateCollectionLocations < ActiveRecord::Migration
  def change
    create_table :collection_locations do |t|
      t.string :year
      t.string :location
      t.string :description
      t.integer :value

      t.timestamps
    end
  end
end
