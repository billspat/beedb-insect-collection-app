class AddLocationToCollectionEvent < ActiveRecord::Migration
  def change
    add_column :collection_events, :location, :string
  end
end
