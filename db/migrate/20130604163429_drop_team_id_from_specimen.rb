class DropTeamIdFromSpecimen < ActiveRecord::Migration
  def up
    remove_column :specimens, :team_id
  end

  def down
    add_column :specimens, :team_id, :integer
  end
end
