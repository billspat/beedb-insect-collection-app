class CreateTreatments < ActiveRecord::Migration
  def change
    create_table :treatments do |t|
      t.string :name
      t.string :treatment_code
      t.integer :year
      t.boolean :active
      t.text :notes

      t.timestamps
    end
  end
end
