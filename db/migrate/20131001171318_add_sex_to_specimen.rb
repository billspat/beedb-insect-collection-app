class AddSexToSpecimen < ActiveRecord::Migration
  def change
    add_column :specimens, :sex, :string
  end
end
