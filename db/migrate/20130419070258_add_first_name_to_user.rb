class AddFirstNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :affiliation, :string
    add_column :users, :position, :string
    add_column :users, :contact, :text
    add_column :users, :added_by_id, :integer
    add_column :users, :notes, :text
  end
end
