class AddSpeciesToSpecimen < ActiveRecord::Migration
  def change
    add_column :specimens, :species, :string
    add_column :specimens, :species_id, :integer
    add_column :specimens, :taxa, :string
  end
end
