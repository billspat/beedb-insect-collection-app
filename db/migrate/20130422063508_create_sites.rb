class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.integer :team_id
      t.integer :location_id
      t.float :lat
      t.float :long
      t.float :elevation
      t.string :name
      t.string :treatment
      t.boolean :active
      t.text :note

      t.timestamps
    end
  end
end
