class DropSpeciesFromSpecimen < ActiveRecord::Migration
  
  def up
     remove_column :specimens, :species
  end

  def down
    add_column  :specimens, :species, :string
  end
  
end
