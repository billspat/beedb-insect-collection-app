class AddTempNameToSpecies < ActiveRecord::Migration
  def change
    add_column :species, :tempname, :string
  end
end
