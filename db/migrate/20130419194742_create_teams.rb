class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.string :affiliation
      t.string :url
      t.text :notes

      t.timestamps
    end
  end
end
