class FixHostplant < ActiveRecord::Migration
  def change
    create_table :hostplants do |t|
      t.string :common_name
      t.text :description
      t.string :taxonomic_family
      t.string :genus
      t.string :species
      t.string :synonyms

      t.timestamps
      
    end
  end
  
end
