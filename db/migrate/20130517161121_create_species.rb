class CreateSpecies < ActiveRecord::Migration
  def change
    create_table :species do |t|
      t.string :taxnomic_family
      t.string :taxonomic_order
      t.string :genus
      t.string :species
      t.string :sub_species
      t.string :common_name
      t.string :common_synonyms
      t.text :notes

      t.timestamps
    end
  end
end
