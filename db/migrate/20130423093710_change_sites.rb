class ChangeSites < ActiveRecord::Migration
  def up
    rename_column :sites, :lat, :latitude
    rename_column :sites, :long, :longitude
  end

  def down
    rename_column :sites, :latitude, :lat
    rename_column :sites, :longitude, :long 
  end
end
