class DropLocationFromCollectionEvent < ActiveRecord::Migration
  def up
    remove_column :collection_events, :location
    
  end

  def down
    add_column :collection_events, :location, :string
  end
end
