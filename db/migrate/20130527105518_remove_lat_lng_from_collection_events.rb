class RemoveLatLngFromCollectionEvents < ActiveRecord::Migration
  def up
     remove_column :collection_events, :lat
     remove_column :collection_events, :lng
     remove_column :collection_events, :distance_from_zero
     
  end

  def down
    add_column  :collection_events, :lat, :float
    add_column  :collection_events, :lng, :float
    add_column  :collection_events, :distance_from_zero, :integer
  end

end
