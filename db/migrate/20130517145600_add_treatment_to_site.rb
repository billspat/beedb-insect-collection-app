class AddTreatmentToSite < ActiveRecord::Migration
  def change
    add_column :sites, :treatment, :reference
  end
end
