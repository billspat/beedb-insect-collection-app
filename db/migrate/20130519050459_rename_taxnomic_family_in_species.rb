class RenameTaxnomicFamilyInSpecies < ActiveRecord::Migration
  def change
    rename_column :species, :taxnomic_family, :taxonomic_family
  end

end
