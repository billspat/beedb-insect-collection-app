class AddTreatmentIdToSite < ActiveRecord::Migration
  def change
    add_column :sites, :treatment_id, :integer
  end
end
