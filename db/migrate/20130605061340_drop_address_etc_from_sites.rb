class DropAddressEtcFromSites < ActiveRecord::Migration
  def change
    add_column :sites, :locality, :string
    remove_column :sites, :address
    remove_column :sites, :city
    remove_column :sites, :region
    remove_column :sites, :postalcode
  end
end
