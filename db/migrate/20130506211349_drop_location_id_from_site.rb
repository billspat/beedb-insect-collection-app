class DropLocationIdFromSite < ActiveRecord::Migration
  def up
    remove_column :sites, :location_id
  end

  def down
    add_column :sites, :location_id, :integer
  end
end
