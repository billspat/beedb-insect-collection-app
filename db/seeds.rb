# ICP application starting values
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

# Environment variables (ENV['...']) are set in the file config/application.yml via figaro gem

puts 'First TEAM'
team = Team.find_or_create_by_name :name => ENV['TEAM_NAME'].dup, :affiliation=> ENV['TEAM_AFFIL'].dup, :url => ENV['TEAM_URL'], :notes =>'Primary Team'

puts 'ROLES'
YAML.load(ENV['ROLES']).each do |role|
  Role.find_or_create_by_name({ :name => role }, :without_protection => true)
  puts 'role: ' << role
end


puts 'DEFAULT USERS'
user = User.find_or_create_by_email :firstname => ENV['ADMIN_FIRSTNAME'].dup, :lastname =>ENV['ADMIN_LASTNAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
puts 'user: ' << user.name
# user.confirm!
user.add_role :admin
user.team = team
team.add_manager(user)
team.save
user.save!

puts 'TREATMENT CODES FOR SITES'

Treatment.create({name: 'Objective 1', treatment_code: 'obj1', year: 2013, active: true})
Treatment.create({name: 'Objective 2', treatment_code: '', year: 2013, active: true})
Treatment.create({name: 'Objective 3', treatment_code: '', year: 2013, active: true})
Treatment.create({name: 'Other', treatment_code: 'other', year: 2013, active: true})

# species list
puts 'SPECIES IMPORT'
ItisImport.import(ENV['BEEFILE'])

# host plants
puts 'HOST PLANTS'
hostplants = [
  ["Vaccinium corymbosum","Northern highbush blueberry"],
  ["Vaccinium virgatum","Rabbit-eye blueberry"],
  ["Malus domestica","Apple"],
  ["Prunus avium","Sweet Cherry"],
  ["Prunus cerasus","Tart Cherry"],
  ["Citrullus lanatus","Watermelon"],
  ["Cucurbita pepo","Pumpkin or Squash"],
  ["Prunus dulcus","Almond"],
  ["Rubus idaeus","Red raspberry"] ]

hostplants.each do |hp|
  hp_genus,hp_species = hp[0].split(" ")
  Hostplant.create(:common_name => hp[1], :genus => hp_genus, :species => hp_species)
end

